<div id="underthecocotree-laravel-chat">
    <div class="content">
        <h1>{{ $room }}</h1>
        <div id="message-container">
            <ul id="messages"></ul>
            <div id="online-users">
                <h2>Online Users</h2>
                <ul></ul>
            </div>
        </div>
        <div>
            <span id="status">Press enter to send...</span>
            <form action="" id="chat-form">
                <input id="message" autocomplete="off" placeholder="type your message" /><button>Send</button>
            </form>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="/underthecocotree/laravel-chat/css/chat.css">
<script src="https://broadcast.dev:3000/socket.io/socket.io.js"></script>
<script>
    var laravel_chat = {
        room: "{{ $room }}",
        user: "{{ $user }}"
    };
</script>
<script src="/underthecocotree/laravel-chat/js/chat.js"></script>