// node socket.js
// A nodejs server that uses socketio and express to read js broadcast events

// TODO: Disable spamming
// https://www.npmjs.com/package/socket-anti-spam

var fs = require('fs');
var privateKey = fs.readFileSync('/usr/share/nginx/certs/server.key', 'utf8');
var certificate = fs.readFileSync('/usr/share/nginx/certs/server.crt', 'utf8');
var credentials = {
    key: privateKey,
    cert: certificate
};

var app = require('express')();
// var http = require('http').Server(app);
var https = require('https').Server(credentials, app);
var io = require('socket.io')(https);
var Redis = require('ioredis');
var redis = new Redis();

// list of users
var users = [];

var debug = process.argv.indexOf('--debug') != -1 ? true : false;

// https://laracasts.com/lessons/broadcasting-events-in-laravel-5-1

redis.subscribe('test-channel', function(err, count) {
    //
});

// connect/disconnect mesages
io.on('connection', function(socket) {
    if (debug)
        console.log('a user connected');

    // disconnect
    socket.on('disconnect', function() {
        if (debug)
            console.log('user disconnected');
    });

    // join room
    socket.on('join', function(room) {
        if (debug)
            console.log(room.user + ' joined ' + room.room);

        socket.join(room.room);

        // initialize array if it doesn't exist
        if (typeof users[room.room] == 'undefined') {
            users[room.room] = [];
        }

        // update users
        users[room.room].push(room.user);
        room.users = users[room.room];

        console.log(users[room.room]);

        io.sockets.in(room.room).emit(room.room + ' joined', room);
    });

    // leave room
    socket.on('leave', function(room) {
        if (debug)
            console.log(room.user + ' left ' + room.room);
          
        socket.leave(room.room);

        if (typeof users[room.room] != 'undefined') {
            // update users
            var index = users[room.room].indexOf(room.user);
            if (index != -1) {
                users[room.room].splice(index, 1);
            }

            room.users = users[room.room];

        }



        io.sockets.in(room.room).emit(room.room + ' left', room);
    });

    // send message to room
    socket.on('send', function(data) {
        if (debug)
            console.log('sending message: ', data);

        io.sockets.in(data.room).emit(data.room, data.user + ': ' + data.message);
        //  io.emit(channel, message);
    });

});

// redis message received form laravel
redis.on('message', (function(channel, message) {
    if (debug)
        console.log(message);

    message = JSON.parse(message);

    io.emit(channel + ':' + message.event, message.data);
}));

// listen as an https server
https.listen(3000, function() {
    console.log('listening on *:3000');
});