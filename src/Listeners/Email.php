<?php

namespace Underthecocotree\LaravelChat\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Underthecocotree\LaravelChat\Events\UserHasRegistered;

class Email
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasRegistered  $event
     * @return void
     */
    public function welcome(UserHasRegistered $event)
    {
        var_dump('The User ' . $event->user . ' has registered. Fire of an email.');
    }
}
