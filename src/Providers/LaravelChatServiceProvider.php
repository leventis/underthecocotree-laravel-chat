<?php

namespace Underthecocotree\LaravelChat\Providers;

use Illuminate\Support\ServiceProvider;

class LaravelChatServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'chat');
        $this->publishes([
        __DIR__.'/../resources/assets' => public_path('underthecocotree/laravel-chat'),
    ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('chat', 'Underthecocotree\LaravelChat\Chat');
    }
}
