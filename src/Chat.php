<?php

namespace Underthecocotree\LaravelChat;

/**
 *    Return chat page
 */
class Chat
{
    /**
     * @param string $room  Name of the room, defaults to room
     * @param string $user Name of the user, defaults to Anonymous
     * 
     * return view
     */
    public function standalone($room = 'room', $user = 'Anonymous')
    {
        return view('chat::chat', compact('room', 'user'));
    }

    /**
     * @param string $room  Name of the room, defaults to room
     * @param string $user Name of the user, defaults to Anonymous
     * 
     * * return view
     */
    public function partial($room = 'room', $user = 'Anonymous')
    {
        return view('chat::partial-chat', compact('room', 'user'));
    }
}
