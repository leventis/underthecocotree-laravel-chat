# Underthecocotree Laravel-Chat

## [Last] - 2015-12-15
### Added
	- Unable to connect to server message
	
## [2249333] - 2015-12-14
### Changed
	- Style, layout and css

## [9c7f90f] - 2015-12-14
### Added 
	- NPM instructions

## [144fa02] - 2015-12-14
### Fixed
	- Removed require autoload

## [4d14a03] - 2015-12-14
	- socket.js debug

## [ae7bde7] - 2015-12-14
## Added
	- Roomname title
	- A list of users that are in the channel
	- joined left room message

## [aca64e6] - 2015-12-7
## Added
	- A User to be associated to a message

## [fbabd48] - 2015-12-7
## Added
	- Pass room name through the facade, Chat.php
	- socketio.js - added a multiple rooms and segregation of messages


[Last]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/all 
[9c7f90f]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/9c7f90f6d7e6cb2f11d96cd4c939661d949e21b1
[144fa02]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/144fa02755d93c135d3a2bbc8eedf75203815481
[4d14a03]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/4d14a0332b1334f05a70a0335fd3bb2a9ae53442
[ae7bde7]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/ae7bde7275f954d77273468df4d41627d70d5f57
[aca64e6]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/aca64e64447c2199977e491bb725be28d8f17d12
[fbabd48]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/fbabd48f09bb11e4c1d5ae6d38b3610502929560
[c92d7e4]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/c92d7e466debfbc0606720e4334864d63a2b50eb
[aac84a2]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/aac84a256eaac26f2430f7c681a95fdc9c22d6b9
[65db544]: https://bitbucket.org/leventis/underthecocotree-laravel-chat/commits/65db5447f5c4d0c4b154c5cf71badf2d2804ba1c