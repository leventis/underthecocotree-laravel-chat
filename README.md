# Under the coco tree - Chat module for laravel

## Install

### config/app.php
	
	// providers
	Underthecocotree\LaravelChat\Providers\LaravelChatServiceProvider::class,

	// aliases
	'Chat' => Underthecocotree\LaravelChat\Facades\Chat::class,
	
### EventServiceProvider
Use if you want to fire events for socket, not needed for chat

   protected $listen = [
        'Underthecocotree\LaravelChat\Events\UserHasRegistered' => [
            'Underthecocotree\LaravelChat\Listeners\Email@welcome',
        ],

### Artisan publish views

	artisan vendor:publish

	## use force if you want to overwrite
	artisan vendor:publish --force

## NPM Dependencies
	
	npm install express ioredis redis socket.io --save

## Config
config/broadcasting.php

    'default' => env('BROADCAST_DRIVER', 'redis'),

## Running
### Server
Will need a monit to keep the service going in case it crashes

	node assets/js/socket.js

	# debug mode
	node underthecocotree/laravel-chat/src/resources/assets/js/socket.js --debug
	
### Chat frontend
Use in route.php as a standalone

	// pass a room name to the facade - default is room
	Route::get('/chat', function () {
		return Chat::standalone();
	});

	Route::get('/standalone-room', function () {
		return Chat::standalone('standalone-room');
	});

	// Pass a username to the facade, default is Anonymous
	// $user = \Auth::user();
	// return Chat::standalone('standalone-room', $user->name);
	return Chat::standalone('standalone-room', 'Iordanis');
	
Use as a partial in a blade template
	
	// defautl name is room
	{!! Chat::partial() !!} 

	// provide a name for the room
	{!! Chat::partial('new-room') !!}

	or @include('chat::partial-chat')
